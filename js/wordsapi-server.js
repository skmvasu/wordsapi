/* Wordsapi server */

/* includes */
var express = require('express');


function WordsAPIServer() {}

WordsAPIServer.DEFAULT_PORT = 8080;

WordsAPIServer.startServer = function (port) {
	if(port == null)
		port = WordsAPIServer.DEFAULT_PORT;
	WordsAPIServer.app = express();
	WordsAPIServer.initHandlers();
	WordsAPIServer.app.listen(port);
};

WordsAPIServer.initHandlers = function() {
	// say, i have an array of objects defining { path: , method: , handler: }
	var meaningFetch = {
		path: '/word/:word',
		method: 'get',
		handler: function(req, res) {
			var word = req.params.word;
			if(word == null || word === '') {
				res.statusCode = 404;
				res.json({
					err: 'The word param is empty. For example: http://wordsapi.org/word/conscience.'
				});
			} else {
				WordsAPILookupEngine.lookup(word, function (wordInfoJson, word) {
					res.set('Content-Type', 'application/json');
					res.send(wordInfoJson);
				});
			}
		}
	};

	// foreach to loop thru all the handlers
	var handlerObj = meaningFetch;
	switch(handlerObj.method) {
		case 'get':
			WordsAPIServer.app.get(handlerObj.path, handlerObj.handler);
			break;
	}
};

