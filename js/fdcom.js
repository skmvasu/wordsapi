/* FreeDictionary.com parser machine */


var FreeDictionaryDotCom = {
	dictUrl: 'http://thefreedictionary.com',
	parse: function (word, raw) {

		var wordInfo = {};

		var pump = function(rawText) {
			const DIVDEFNSTR = "<div id=Definition";

			var curChar = '';
			var ctr = rawText.indexOf(DIVDEFNSTR);
			ctr = ctr == -1 ? 0 : ctr;
			var length = rawText.length;
			var matchResult = null;
			var sectionStartIdx = 0;
			var sectionEndIdx = 0;
			var section = '';

			var state = 0;

			const STATE_START = 0;
			const STATE_DEFNDIVFOUND = 1;
			const STATE_SPCFDSECNFOUND = 2;
			const STATE_SECNENDFOUND = 3;
			const STATE_END = 10;

			var matches = function(string) {
				var substr = rawText.substr(ctr, string.length)
				return {  result: (rawText.substr(ctr, string.length) === string), nextIdx: ctr + string.length };
			};

			while(1) {
				if(ctr >= length)
					break;

				curChar = rawText[ctr];

				switch(state) {
					case STATE_START:
						if((matchResult = matches(DIVDEFNSTR)).result) {
							ctr = matchResult.nextIdx;
							state = STATE_DEFNDIVFOUND;
						} else {
							ctr++;
						}
						break;
					case STATE_DEFNDIVFOUND:
						if((matchResult = matches("<section data-src=hm")).result) {
							sectionStartIdx = ctr;
							ctr = matchResult.nextIdx;
							state = STATE_SPCFDSECNFOUND;
						} else {
							ctr++;
						}
						break;
					case STATE_SPCFDSECNFOUND:
						if((matchResult = matches("</section>")).result) {
							ctr = matchResult.nextIdx;
							sectionEndIdx = ctr;
							state = STATE_SECNENDFOUND;
						} else {
							ctr++;
						}
						break;
					case STATE_SECNENDFOUND:
						section = rawText.substr(sectionStartIdx, sectionEndIdx - sectionStartIdx);
						state = STATE_END;
						break;
				}

				if(state == STATE_END)
					break;

			}

			// start parsing the american heritage section.
			var sectionNode = $(section);

			var actualWord = '';
			var meanings = [];
			var meaningsByPart = [];

			var parseSubDescriptionList = function(subDescListNode) {
				var childNode = null;
				var subDescriptionParts = [];
				var illustrationParts = [];

				for (var j = 0; j < subDescListNode.childNodes.length; j++) {
					childNode = subDescListNode.childNodes[j];
					if(childNode.nodeName === 'B') {
						if(!$(childNode).text().match(/(\d|\w)\./)) {
							subDescriptionParts.push($(childNode).text());
						}
					} else if(childNode.nodeName === 'SPAN' && childNode.className === 'hvr') {
						subDescriptionParts.push($(childNode).text());
					} else if(childNode.nodeName === 'I') {
						subDescriptionParts.push($(childNode).text());
					} else if(childNode.nodeName === 'SPAN' && childNode.className === 'illustration') {
						illustrationParts.push($(childNode).text());
					} else if(childNode.nodeName === '#text') {
						subDescriptionParts.push(childNode.textContent);
					}

				};

				// Subdescription ending with bug. strip ending with :.
				var subDescription = subDescriptionParts.join('').trim();
				if(subDescription[subDescription.length - 1] === ':') {
					subDescription = subDescription.slice(0, subDescription.length - 1);
				}

				return {
					subDescription: subDescriptionParts.join('').trim(),
					illustration: illustrationParts.join('').trim()
				}
			};

			var parseDescriptionList = function(descListNode) {
				var childNode = null;
				var descriptionParts = [];
				var illustrationParts = [];
				var subDescriptions = [];

				for (var i = 0; i < descListNode.childNodes.length; i++) {
					childNode = descListNode.childNodes[i];
					if(childNode.nodeName === 'B') {
						if(!$(childNode).text().match(/(\d|\w)\./)) {
							descriptionParts.push($(childNode).text());
						}
					} else if(childNode.nodeName === 'SPAN' && childNode.className === 'hvr') {
						descriptionParts.push($(childNode).text());
					} else if(childNode.nodeName === 'I') {
						descriptionParts.push($(childNode).text());
					} else if(childNode.nodeName === 'SPAN' && childNode.className === 'illustration') {
						illustrationParts.push($(childNode).text());
					} else if(childNode.nodeName === 'DIV' && childNode.className === 'sds-list') {
						subDescriptions.push(parseSubDescriptionList(childNode));
					} else if(itm.nodeName === 'A') {
						plainDescriptionParts.push($(itm).text());
					} else if(childNode.nodeName === '#text') {
						descriptionParts.push(childNode.textContent);
					}

				};

				return {
					description: descriptionParts.join('').trim(),
					illustration: illustrationParts.join('').trim(),
					subDescriptions: subDescriptions
				}
			};

			var parsePartSeg = function(partSegNode) {
				var partsOfSpeech = [];
				var descriptions = [];
				// usually tenses of the word
				var alternativeWords = [];


				for (var i = 0; i < partSegNode.childNodes.length; i++) {
					var itm = partSegNode.childNodes[i];
					if(itm.nodeName === 'I') {
						partsOfSpeech.push($(itm).text());
					} else if(itm.nodeName === 'DIV' && (itm.className === 'ds-list' || itm.className === 'ds-single')) {
						descriptions.push(parseDescriptionList(itm));
					} else if(itm.nodeName === 'DIV' && (itm.className === 'ds-list' || itm.className === 'ds-single')) {
						descriptions.push(parseDescriptionList(itm));
					} else if(itm.nodeName === 'B') {
						alternativeWords.push($(itm).text());
					} else if(itm.nodeName === '#text') {
						//alternativeWords.push($(itm).text());
					}
				};

				if(alternativeWords.length != 0) {
					descriptions.push({ 
						alternativeWords: alternativeWords,
					});
				}

				return {
					partsOfSpeech: partsOfSpeech,
					descriptions: descriptions
				}
			};

			var defnChildren = $(sectionNode).children();

			var constructWordInfo = function() {
				// finish contructing the meaning by num here
				if(actualWord != '' && meaningsByPart.length != 0) {
					meanings.push({
						actual: actualWord,
						meaningsByPart: meaningsByPart
					});
					meaningsByPart = [];
				}
			};

			for (var i = 0; i < defnChildren.length; i++) {
				var itm = defnChildren[i];
				if(itm.nodeName === 'H2') {
					constructWordInfo();
					actualWord = $(itm).text();
				} else if(itm.nodeName === 'DIV' && itm.className === 'pseg') {
					meaningsByPart.push(parsePartSeg(itm));
				}
			};

			// construct the last word meanings
			constructWordInfo();

			return {
				meanings: meanings,
				actualWord: actualWord
			};
	
		};

		wordInfo = pump(raw);
		wordInfo.givenWord = word;
		return wordInfo;
	},

	getStdWordInfo: function(word) {
		
	}

};

