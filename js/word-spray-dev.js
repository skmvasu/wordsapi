/* Word spray dev */

//createSessionDirs();

function run() {
	
	var text = readFile(ws_srcTxt);
	//processText(text);
	var wordList = quickProcessText(text);
	requestQueueManager(wordList, 'http://thefreedictionary.com');
	startMonitor(wordList);
}

function testFdComParse() {
	var wordInfo = FreeDictionaryDotCom.parse('none', readFile('C:/Users/karti/workspace/word-spray/1422682361578/raw/none.html', { encoding: 'utf-8'}));
}

function startOnlyMonitor() {
	var text = readFile(ws_srcTxt);
	var wordList = quickProcessText(text);
	startMonitor(wordList);
}

function runWordsAPIEngine() {
	WordsAPILookupEngine.setupDirs();
	RedisService.initialize();
	WordsAPIServer.startServer(parseInt(document.getElementById("portNo")));
}