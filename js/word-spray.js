/* Word-spray main JS */

/* includes */
var fs = require('fs');
var redis = require('redis');
var winston = require('winston');

/* constants */
const PATH_SEP = '/';
const HTML_EXT = '.html';
const JSON_EXT = '.json';
const MONITOR_TIMEOUT_MS = 60 * 1000;

var ws_SessionDir = 'default';
var ws_rawFilesDir = ws_SessionDir + PATH_SEP + 'raw';
var ws_dbDir = ws_SessionDir + PATH_SEP + 'db';
var ws_wordsDir = ws_dbDir + PATH_SEP + 'words';
var ws_project = 'eden';
var ws_textDir = ws_SessionDir + PATH_SEP + 'text';
var ws_projectDir = ws_textDir + PATH_SEP + ws_project;
var ws_srcTxt = 'eden.txt';

var ws_monitorTimer = null;

var lookup = new (winston.Logger)({
transports: [
  new (winston.transports.File)({ filename: ws_SessionDir + PATH_SEP + 'lookup.log' })
]
});

var logger = new (winston.Logger)({
transports: [
  new (winston.transports.File)({ filename: ws_SessionDir + PATH_SEP + 'wordsapi.log' })
]
});
function getRawSync(url) {
	var retContent;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, false);
	xhr.send();
	return xhr.responseText;
}

function getRaw(url, word, callback) {
	var retContent;
	var xhr = new XMLHttpRequest();
	xhr.open('GET', url, true);
	xhr.onload = function() {
		callback(this.responseText, word, false);
	};
	xhr.onerror = function() {
		callback('', word, true);
	};
	xhr.onabort = function() {
		callback('', word, true);
	};
	xhr.send();
}

function writeRaw(fileName, data) {
	fs.writeFile(ws_rawFilesDir + PATH_SEP + fileName, data, function (argument) {});
}

function writeFile(fileName, data) {
	fs.writeFileSync(fileName, data);
}

function readFile(fileName) {
	return fs.readFileSync(fileName, { encoding: 'utf-8'}).replace(/\r/g, '');
}

function processText(text) {
	var textStruct = {};
	var paragraphs = text.split("\n");
	var rawStage1File = ws_projectDir + PATH_SEP + 'raw_stg1.json';

	textStruct.paragraphs = {};
	for (var i = paragraphs.length - 1; i >= 0; i--) {
		textStruct.paragraphs[i] = { raw: paragraphs[i]};
	};
	writeFile(rawStage1File, JSON.stringify(textStruct, null, 2));

}

function quickProcessText(text) {
	var qpRawStage1File = ws_projectDir + PATH_SEP + 'qpRawstg1.json';
	var wordList = wordPump(text);
	writeFile(qpRawStage1File, JSON.stringify(wordList, null, 2));
	return wordList;
}

function wordPump(text) {
	var alphabets = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	var curChar = '';
	var ctr = 0;
	var length = text.length;
	var wordList = [];
	var charStack = [];
	var word = '';
	while(1) {
		if(ctr >= length)
			break;

		curChar = text[ctr];
		if(alphabets.indexOf(curChar) != -1) {
			charStack.push(curChar);
		} else {
			word = charStack.join('').toLowerCase();
			if(wordList.indexOf(word) == -1 && word != '') {
				wordList.push(word);
			}
			charStack = [];
		}
		ctr++;
	}
	return wordList;
}

function createSessionDirs() {
	fs.mkdirSync(ws_SessionDir);
	fs.mkdirSync(ws_dbDir);
	fs.mkdirSync(ws_rawFilesDir);
	fs.mkdirSync(ws_textDir);
	fs.mkdirSync(ws_projectDir);
	fs.mkdirSync(ws_wordsDir);
}

function requestQueueManager(wordList, baseUrl) {
	var reqQueue = [];
	var timer = null;
	const QUEUE_LENGTH = 4;

	var queue = function() {
		if(wordList.length == 0)
			return;
		reqQueue = wordList.splice(0, QUEUE_LENGTH);
		reqQueueCopy = reqQueue.slice();
		reqQueueCopy.forEach(function(item, idx, arr) {
			if(fs.existsSync(ws_rawFilesDir + PATH_SEP + item + HTML_EXT)) {
				notify(item, '', true);
				return;
			}
			getRaw(baseUrl + '/' + item, item, function(rawText, word, skip) {
				notify(word, rawText, skip);
			});
		});
	};

	var skipCtr = 0;

	var notify = function(word, rawText, skip) {
		if(rawText.indexOf('HTTP 403.6 - Forbidden: IP address rejected') != -1) {
			logger.error("Website detected crawling.");
			// wait for 10 minutes crawl another set
			timer = setTimeout(queue, 10 * 60 * 1000);
			return;
		}
		reqQueue.splice(reqQueue.indexOf(word), 1);
		if(!skip) {
			writeFile(ws_rawFilesDir + PATH_SEP + word + HTML_EXT, rawText);
		} else {
			skipCtr++;
		}
		if(reqQueue.length == 0) {
			if(skipCtr == QUEUE_LENGTH) {
				timer = setTimeout(queue, 1000);
				skipCtr = 0;
			}
			else
				timer = setTimeout(queue, 1 * 10 * 1000);
		}
	};

	queue();
}

function startMonitor(wordList) {
	ws_monitorTimer = setTimeout(monitor, MONITOR_TIMEOUT_MS, wordList);
}

function monitor(wordList) {
	for (var i = 0; i < wordList.length; i++) {
		var word = wordList[i];
		if(!fs.existsSync(ws_wordsDir + PATH_SEP + word + JSON_EXT)) {
			if(fs.existsSync(ws_rawFilesDir + PATH_SEP + word + HTML_EXT)) {
				var wordMeaningRawHtml = readFile(ws_rawFilesDir + PATH_SEP + word + HTML_EXT);
				var wordInfo = FreeDictionaryDotCom.parse(word, wordMeaningRawHtml);
				writeFile(ws_wordsDir + PATH_SEP + word + JSON_EXT, JSON.stringify(wordInfo, null, 2));
			}
		}
	};
	ws_monitorTimer = setTimeout(monitor, MONITOR_TIMEOUT_MS, wordList);
}

function WordsAPILookupEngine() {}

// a sample config for now
WordsAPILookupEngine.lookupConfig = {
	dictionary: FreeDictionaryDotCom

};

// tis is one time
WordsAPILookupEngine.setupDirs = function() {
	logger.debug("Trying to create session directory structure");
	if(!fs.existsSync(ws_SessionDir)) {
		fs.mkdirSync(ws_SessionDir);
		fs.mkdirSync(ws_dbDir);
		fs.mkdirSync(ws_rawFilesDir);
		fs.mkdirSync(ws_wordsDir);
	}
};

// word: word to find its meaning
// callback(raw): called with raw html
WordsAPILookupEngine.lookup = function (word, callback) {
	var onDemandLookup = function(word) {
		getRaw(WordsAPILookupEngine.lookupConfig.dictionary.dictUrl + '/' + word, word, function(rawText, word, skip) {
			// store the raw HTML file
			writeFile(ws_rawFilesDir + PATH_SEP + word + HTML_EXT, rawText);
			// now, parse the HTML, store JSON object, and respond
			var wordInfo = WordsAPILookupEngine.lookupConfig.dictionary.parse(word, rawText);
			writeFile(ws_wordsDir + PATH_SEP + word + JSON_EXT, JSON.stringify(wordInfo, null, 2));
			// add to cache
			RedisService.addJSON(word, JSON.stringify(wordInfo), function() {
				// log error/ok
				//console.log(arguments);
				logger.debug("Dumping callback arguemnts for onDemandLookup/addJSON: " + arguments);
			});
			callback(JSON.stringify(wordInfo), word);
		});
	};

	var redisLookup = function(word) {
		RedisService.getJSON(word, function(reply, word) {
			if(reply != null) {
				lookup.info("HIT redis " + word);
				callback(reply, word);
			}
			else {
				lookup.info("MISS redis " + word);
				fsLookup(word);
			}
		});
	};

	var fsLookup = function(word) {
		// check redis for file already exists
		// initialize redis with word file names in a list

		// 2nd thought, will have a normal fs.exist here
		if(fs.existsSync(ws_wordsDir + PATH_SEP + word + ".json")) {
			var wordInfoJson = fs.readFileSync(ws_wordsDir + PATH_SEP + word + ".json", { encoding: 'utf8'});
			// add to cache
			RedisService.addJSON(word, wordInfoJson, function() {
				// log error/ok
				//console.log(arguments);
				logger.debug("Dumping callback arguemnts for fsLookup/addJSON: " + arguemnts);
			});
			lookup.info("HIT fs " + word);
			callback(wordInfoJson, word);
		} else {
			lookup.info("MISS fs " + word);
			// we dont have the word in our server, do ondemand lookup from thefreedictionary.com/American Heritage
			onDemandLookup(word);
		}

	};

	redisLookup(word);
};

function RedisService() {}

RedisService.initialize = function() {
	// have redis client created
	RedisService.client = redis.createClient();

	RedisService.client.on("error", function (err) {
        logger.error("Error " + err);
    });
};

RedisService.getJSON = function(word, callback) {
	RedisService.client.get(word, function(err, reply) {
		callback(reply, word, err);
	});
};

RedisService.addJSON = function(word, wordInfoStr, callback) {
	RedisService.client.set(word, wordInfoStr, function(err, reply) {
		callback(err, reply);
	});
};